# Pico Stopwatch

A Raspberry Pi Pico based stopwatch.

## Features

 - 1.3" IPS screen and joystick for menu
 - Start, stop, lap, reset inputs and buttons
 - Polarity selection for all inputs
 - Min, Max, Average lap time display
 - Opto-isolated inputs for up to 24V signals
 - Internal Li-Ion battery and USB charging/power
